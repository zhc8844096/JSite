/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.service;

import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.yili.dao.YiliSocialSecurityInfoDao;
import com.jsite.modules.yili.entity.YiliSocialSecurityInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 社保信息表Service
 * @author liuruijun
 * @version 2020-07-29
 */
@Service
@Transactional(readOnly = true)
public class YiliSocialSecurityInfoService extends CrudService<YiliSocialSecurityInfoDao, YiliSocialSecurityInfo> {

	@Override
	public YiliSocialSecurityInfo get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<YiliSocialSecurityInfo> findList(YiliSocialSecurityInfo yiliSocialSecurityInfo) {
		return super.findList(yiliSocialSecurityInfo);
	}
	
	@Override
	public Page<YiliSocialSecurityInfo> findPage(Page<YiliSocialSecurityInfo> page, YiliSocialSecurityInfo yiliSocialSecurityInfo) {
		return super.findPage(page, yiliSocialSecurityInfo);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(YiliSocialSecurityInfo yiliSocialSecurityInfo) {
		super.save(yiliSocialSecurityInfo);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(YiliSocialSecurityInfo yiliSocialSecurityInfo) {
		super.delete(yiliSocialSecurityInfo);
	}
	
}